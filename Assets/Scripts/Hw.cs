using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class Hw : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;
    bool isFloat = true;

    public void MyStart()
    {
        UpdateText();
        if (isFloat)
        {
            float[] array = CreateArray<float>();
            ProcessArrayRef(ref array);
            ProcessArrayOut(out array);
        }
        else
        {
            int[] array = CreateArray<int>();
            ProcessArrayRef(ref array);
            ProcessArrayOut(out array);
        }
    }

    public void UpdateText()
    {
        text.text = isFloat ? "float" : "int";
    }

    public void ChangeBool()
    {
        isFloat = !isFloat;
        UpdateText();
    }
    public static T[] CreateArray<T>() where T : struct
    {
        T[] array = new T[10000];

        array[0] = (T)Convert.ChangeType(2, typeof(T));

        for (int i = 1; i < array.Length; i++)
        {
            try
            {
                checked
                {
                    array[i] = (T)Convert.ChangeType(Math.Pow(Convert.ToDouble(array[i - 1]), 2), typeof(T));
                    CheckOverflow(array[i]);
                    Debug.Log(array[i]);
                }
            }
            catch (OverflowException)
            {
                Debug.Log("Create - Overflow exception occurred.");
                break;
            }
        }

        return array;
    }

    public static void ProcessArrayRef<T>(ref T[] array) where T : struct
    {
        for (int i = 1; i < array.Length; i++)
        {
            try
            {
                checked
                {
                    array[i] = (T)Convert.ChangeType(Math.Pow(Convert.ToDouble(array[i - 1]), 2), typeof(T));
                    CheckOverflow(array[i]);
                    Debug.Log(array[i]);
                }
            }
            catch (OverflowException)
            {
                Debug.Log("Ref - Overflow exception occurred.");
                break;
            }
        }

        SaveStructToFile(new MyStruct<T>(array));
    }

    public static void ProcessArrayOut<T>(out T[] array) where T : struct
    {
        array = CreateArray<T>();
        for (int i = 1; i < array.Length; i++)
        {
            try
            {
                checked
                {
                    array[i] = (T)Convert.ChangeType(Math.Pow(Convert.ToDouble(array[i - 1]), 2), typeof(T));
                    CheckOverflow(array[i]);
                    Debug.Log(array[i]);
                }
            }
            catch (OverflowException)
            {
                Debug.Log("Out - Overflow exception occurred.");
                break;
            }
        }

        SaveStructToFile(new MyStruct<T>(array));
    }

    public static void SaveStructToFile<T>(MyStruct<T> myStruct) where T : struct
    {
        string json = JsonConvert.SerializeObject(myStruct);
        File.WriteAllText("Assets\\File.json", json);
    }

    public static void CheckOverflow<T>(T value)
    {
        if (value is float && float.IsInfinity(float.Parse(value.ToString())))
        {
            throw new OverflowException("Overflow exception occurred.");
        }
    }
}
