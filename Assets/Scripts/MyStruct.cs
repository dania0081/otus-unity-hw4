using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct MyStruct<T> where T : struct
{
    public T[] Array { get; set; }

    public MyStruct(T[] array)
    {
        Array = array;
    }
}
